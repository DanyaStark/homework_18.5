﻿#include <iostream>
using namespace std;

class Stack
{
private:

    int top = -1;
    int n;
    int* stack = new int[n];

public:

    void massiveLength(int x)
    {
        n = x;
    }

    void pop()
    {
        if (top >= 0)
        {
            cout << "Popped element's value: " << stack[top] << endl;
            top--;
        }
        else
        {
            cout << "No elements in stack" << endl;
        }
    }

    void push(int x)
    {
        if (top <= n)
        {
            top++;
            stack[top] = x;
        }
        else
        {
            cout << "Stack is full" << endl;
        }
    }

    void display()
    {
        if (top >= 0)
        {
            cout << "Stack elements are: " << endl;
            for (int i = top; i >= 0; i--)
            {
                cout << stack[i] << ", ";
            }
            cout << endl;
        }
        else
        {
            cout << "Stack is empty" << endl;
        }
    }
};

int main()
{
    int ch = 0;
    int x = 0;
    Stack stk;

    cout << "1) Push in stack" << endl;
    cout << "2) Pop from stack" << endl;
    cout << "3) Display stack" << endl;
    cout << "4) Exit" << endl;

    cout << "How much space in massive needed?" << endl;
    cin >> x;
    stk.massiveLength(x);

    do
    {
        cout << "Enter choise:" << endl;
        cin >> ch;
        switch (ch)
        {
        case 1:
        {
            cout << "Enter value to push: " << endl;
            cin >> x;
            stk.push(x);
            break;
        }
        case 2:
        {
            stk.pop();
            break;
        }
        case 3:
        {
            stk.display();
            break;
        }
        case 4: 
        {
            break;
        }
        default:
        {
            cout << "Invalid option" << endl;
        }
        }
    }
    while (ch!=4);
    return 0;
}